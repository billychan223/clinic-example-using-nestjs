import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateConsultationRequestDto } from './dto/createConsultationRequest.dto';
import { Consultation } from './consultation.entity';
import { ConsultationService } from './consultation.service';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { RolesAllowed } from 'src/auth/decorators/roles.decorator';
import { Roles } from 'src/auth/Roles';

@ApiTags('Consultation')
@Controller('consultations')
@UseGuards(JwtAuthGuard, RolesGuard)
export class ConsultationController {
  constructor(private readonly consultationService: ConsultationService) {}

  @Post()
  @RolesAllowed(Roles.ADMIN)
  create(
    @Body() createConsultationDto: CreateConsultationRequestDto,
  ): Promise<Consultation> {
    return this.consultationService.create(createConsultationDto);
  }

  @Get()
  @RolesAllowed(Roles.ADMIN, Roles.USER)
  findAll(): Promise<Consultation[]> {
    return this.consultationService.findAll();
  }

  @Get(':id')
  @RolesAllowed(Roles.ADMIN, Roles.USER)
  findOne(@Param('id') id: string): Promise<Consultation> {
    return this.consultationService.findOne(id);
  }

  @Delete(':id')
  @RolesAllowed(Roles.ADMIN)
  remove(@Param('id') id: string): Promise<void> {
    return this.consultationService.remove(id);
  }
}
