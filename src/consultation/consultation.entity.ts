import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Users } from '../users/users.entity';

@Entity()
export class Consultation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  doctorName: string;

  @Column()
  patientName: string;

  @Column()
  diagnosis: string;

  @Column()
  medication: string;

  @Column()
  consultationFee: number;

  @Column()
  dateTime: Date;

  @Column({ nullable: false, default: false })
  hasFollowup: boolean;

  @ManyToOne(
    () => Users,
    user => user.consultations,
  )
  user: Users;
}
