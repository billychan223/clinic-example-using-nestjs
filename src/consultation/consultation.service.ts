import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Consultation } from './consultation.entity';
import { CreateConsultationRequestDto } from './dto/createConsultationRequest.dto';

@Injectable()
export class ConsultationService {
  constructor(
    @InjectRepository(Consultation)
    private readonly consultationRepo: Repository<Consultation>,
  ) {}

  create(createNewsDto: CreateConsultationRequestDto): Promise<Consultation> {
    const consultation = new Consultation();
    consultation.consultationFee = createNewsDto.consultationFee;
    consultation.dateTime = createNewsDto.dateTime;
    consultation.diagnosis = createNewsDto.diagnosis;
    consultation.doctorName = createNewsDto.doctorName;
    consultation.hasFollowup = createNewsDto.hasFollowup;
    consultation.medication = createNewsDto.medication;
    consultation.patientName = createNewsDto.patientName;

    return this.consultationRepo.save(consultation);
  }

  async findAll(): Promise<Consultation[]> {
    return this.consultationRepo.find();
  }

  async findOne(id: string): Promise<Consultation> {
    return this.consultationRepo.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.consultationRepo.delete(id);
  }
}
