import {
  IsBoolean,
  IsDate,
  IsDateString,
  IsNumber,
  IsString,
  MaxLength,
} from 'class-validator';

export class CreateConsultationRequestDto {
  @IsString()
  @MaxLength(64)
  doctorName: string;

  @IsString()
  @MaxLength(64)
  patientName: string;

  @IsString()
  diagnosis: string;

  @IsString()
  medication: string;

  @IsNumber()
  consultationFee: number;

  @IsDateString()
  dateTime: Date;

  @IsBoolean()
  hasFollowup: boolean;
}
