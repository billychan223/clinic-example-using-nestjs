import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import sha1 from 'crypto-js/sha1';
import { UsersService } from '../users/users.service';
import { Users } from 'src/users/users.entity';
import { RegisterRequestDto } from './dto/registerRequest.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    console.log(email);
    const user = await this.usersService.findByEmail(email);

    if (user && user.password === sha1(password).toString()) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: Users) {
    console.log(user);
    const payload = { email: user.email, sub: user.id };
    return {
      accessToken: this.jwtService.sign(payload),
    };
  }

  async register(req: RegisterRequestDto) {
    const user = {
      ...req,
      password: sha1(req.password).toString(),
    } as Users;

    try {
      const result = await this.usersService.createUser(user);
      const payload = { email: req.email, sub: result.id };
      return {
        accessToken: this.jwtService.sign(payload),
      };
    } catch (e) {
      throw e;
    }
  }
}
