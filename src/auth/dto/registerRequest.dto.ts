import {
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Roles } from '../Roles';

export class RegisterRequestDto {
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  password: string;

  @IsEmail()
  email: string;

  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(256)
  address: string;

  @IsOptional()
  @IsEnum(Roles)
  role: number;

  @IsString()
  @IsNotEmpty()
  phoneNumber: string;
}
