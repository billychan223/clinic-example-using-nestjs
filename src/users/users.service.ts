import { Injectable, ForbiddenException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Users } from './users.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users)
    private readonly usersRepository: Repository<Users>,
  ) {}

  async findByEmail(email: string): Promise<Users | undefined> {
    return this.usersRepository
      .createQueryBuilder('users')
      .where('users.email = :email', { email })
      .getOne();
  }

  async createUser(user: Users): Promise<Users> {
    if ((await this.usersRepository.find({ email: user.email })).length > 0) {
      throw new ForbiddenException('A user with this email already exists.');
    }
    return (await this.usersRepository.insert(user)).raw;
  }
}
