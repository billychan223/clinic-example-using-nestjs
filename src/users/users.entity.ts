import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Roles } from 'src/auth/Roles';
import { Consultation } from 'src/consultation/consultation.entity';

@Entity()
export class Users {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column()
  name: string;

  @Column()
  phoneNumber: string;

  @Column()
  address: string;

  @Column({ default: Roles.USER })
  role: number;

  @OneToMany(
    () => Consultation,
    consultation => consultation.user,
  )
  consultations: Consultation[];
}
