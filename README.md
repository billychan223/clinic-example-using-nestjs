## A simple clinic example use NestJS framework + postgres

## Installation

```bash
$ npm install
```

## Running

```bash
# Set up the environment
$ cp .env.example .env

# Spin up the db
$ docker-compose up -d

# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Documentation

```
$ doc:gen
```

## Things can do more later on

1. add pagination
2. unify the response format
3. Implement Authorization Code Flow + PKCE
